extern crate prost_build;

use std::fs::File;
use std::io::Read;
use std::io::Write;
use std::env;
use std::process::Command;

/// Before we can build the project, we generate the protobuf stuff and
/// some utility code for Mumble's protocol
/// 
/// All of our generated code should go into the `mumble` module, and
/// looks like this:
/// ```rust,ignore
/// pub enum MessageTypes {
///     Version,
///     UdpTunnel,
///     Authenticate,
///     // etc...,
/// }
///
/// pub fn decode_by_id<B>(id: u16, buf: B) -> Result<Message, DecodeError> {
///     // whatever
/// }
/// ```
fn main() {
    // We unwrap EVERYTHING because if anything
    // fails, we should definitely not build.

    let proto_filename = "src/Mumble.proto";
    prost_build::compile_protos(&[proto_filename],
                                &["src/"]).unwrap();

    let mut proto = File::open(proto_filename).unwrap();
    let mut proto_contents = String::new();
    proto.read_to_string(&mut proto_contents).unwrap();

    // this is just "${OUT_DIR}/message_types.rs":
    let rs_filename = format!("{}/{}",
                              env::var("OUT_DIR").unwrap(),
                              "message_types.rs");

    // wrap this up so that 'rs' is closed by the time we run rustfmt
    {
        let mut rs = File::create(&rs_filename).unwrap();

        let mut message_types = Vec::new();
        for line in proto_contents.lines() {
            if line.starts_with("message ") {
                message_types.push(
                    line.split(' ').nth(1).unwrap());
            }
        }

        rs.write_all(b"pub enum MessageType {").unwrap();
        for (index,message_type) in message_types.iter().enumerate() {
            let bytes = format!("{} = {},", message_type, index).into_bytes();
            rs.write_all(&bytes).unwrap();
        }
        rs.write_all(b"}").unwrap();

        rs.write_all(b"\n
        /// Decodes a message of type `id` (use a MessageType as u16)
        /// into buf `buf` using that type's own `decode` method
        pub fn decode_by_id<B>(id: u16, buf: B) -> Box<Message>
            where B: IntoBuf
            {
                match id {").unwrap();
                rs.write_all(&format!("
                    x if x == MessageType::{0} as u16 => Box::new({0}::decode(buf).unwrap()),
                ", "Version").as_bytes()).unwrap();

                rs.write_all(b"
                _ => Box::new(Reject {
                                type_: Some(0),
                                reason: Some(String::from(\"unknown data type\"))
                              })
                }
            }
            ").unwrap();
    } // rs is closed past this point, writes must occur before here

    Command::new("rustfmt").args(&[rs_filename]).status().unwrap();
}
