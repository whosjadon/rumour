extern crate prost;
extern crate bytes;
extern crate tokio;
extern crate tokio_io;
extern crate tokio_tls;
extern crate native_tls;
extern crate os_info;

#[macro_use]
extern crate prost_derive;

use std::net::ToSocketAddrs;
use std::io;
use tokio::net::TcpStream;
use tokio::prelude::*;
use tokio::codec::{Framed,length_delimited};
use native_tls::TlsConnector;
use tokio_tls::TlsStream;
use prost::Message;
use bytes::{ByteOrder, BigEndian};

/// generated from /src/Mumble.proto with the 'prost' crate
pub mod mumble {
    use prost::Message;
    use bytes::IntoBuf;
    include!(concat!(env!("OUT_DIR"), "/mumble_proto.rs"));
    include!(concat!(env!("OUT_DIR"), "/message_types.rs"));
}


const DEFAULT_MUMBLE_PORT: u16 = 64738;

const VERSION_MAJOR: u16 = 1;
const VERSION_MINOR: u8 = 2;
const VERSION_PATCH: u8 = 4;
const VERSION: u32 = ((VERSION_MAJOR as u32) << 16)
                   | ((VERSION_MINOR as u32) << 8)
                   | (VERSION_PATCH as u32);

/// Contains a server to connect to, some options, and the actual connection
/// stream. It also provides methods to manipulate this stream.
pub struct Rumour<'a> {
    pub address: &'a str,
    pub port: Option<u16>,
    pub insecure_server: Option<bool>,
    pub username: &'a str,
    pub password: Option<&'a str>,
    /// The server connection. This is always encrypted.
    socket: Option<TlsStream<TcpStream>>,
}

impl<'a> Default for Rumour<'a> {
    fn default() -> Rumour<'a> {
        Rumour::new()
    }
}

impl<'a> Rumour<'a> {
    //! Synchronously opens a connection to the server indicated in a Rumour
    pub fn connect(&mut self) -> Future {
        let address = self.address.clone();
        let port = self.port.unwrap_or(DEFAULT_MUMBLE_PORT);

        let mut addr_iter = format!("{}:{}", address, port).to_socket_addrs()?;
        // this unwrap should be safe, since any error's surely
        // going to happen in the resolution, not the iterator:
        let addr = addr_iter.next().unwrap(); // TODO: try others on fail
        drop(addr_iter);

        let socket = TcpStream::connect(&addr);
        let mut builder = TlsConnector::builder();
        if self.insecure_server.is_some() && self.insecure_server.unwrap() {
            builder.danger_accept_invalid_certs(true);
        }
        let connector = builder.build().unwrap();
        let connector = tokio_tls::TlsConnector::from(connector);

        let tls_open = socket.and_then(|socket| {
            connector.connect(address, socket).map_err(
                |err| io::Error::new(io::ErrorKind::Other, err)
            )
        });

        let assign_socket = tls_open.and_then(move |socket| {
            let framed_sock = length_delimited::Builder::new()
                .length_field_offset(2)
                .length_field_length(4)
                .num_skip(0)
                .new_framed(socket);
            framed_sock.for_each(|packet| {
                println!("FRAME! - {:?} - !EMARF", packet);
                Ok(())
            })
        });

        //TODO: don't wait. waiting sucks.
        assign_socket.wait()
    }

    fn send_version(&mut self) -> Result<(), io::Error> {
        let osinfo = os_info::get();
        let release = String::from("Rumour");
        let os = format!("{}", osinfo.os_type());
        let os_version = format!("{}", osinfo.version());

        let version_packet = mumble::Version {
            version: Some(VERSION),
            release: Some(release),
            os: Some(os),
            os_version: Some(os_version)
        };

        let mut buf = Vec::with_capacity(version_packet.encoded_len());

        version_packet.encode_length_delimited(&mut buf)?;
        match self.socket {
            Some(ref mut socket) => {
                socket.write_all(&[0u8, mumble::MessageType::Version as u8])?;
                socket.write_all(&buf)
            },
            None => Err(io::Error::new(io::ErrorKind::Other, "No socket open!"))
        }
    }

    pub fn new() -> Rumour<'a> {
        Rumour {
            address: "",
            port: None,
            insecure_server: None,
            username: "",
            password: None,
            socket: None
        }
    }
}
