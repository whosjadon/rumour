extern crate rumour;
use rumour::Rumour;

use std::env;

#[test]
fn connect() -> std::result::Result<(), std::io::Error> {
    let mut insecure: Option<bool> = None;
    let address = env::var("TEST_SERVER");
    let address = match &address {
        Ok(addr) => &addr,
        Err(_) => {
            eprintln!("\nWARNING: this test tries to connect to localhost by default.");
            eprintln!("to change this, set the environment variable TEST_SERVER:");
            eprintln!("$ env TEST_SERVER=example.com cargo test\n");
            insecure = Some(true);
            "127.0.0.1"
        }
    };

    // make sure you've come up with your address before you ::new(), since
    // values are dropped in the reverse order they're created in.
    let mut client = Rumour::new();
    client.address = address;
    client.username = "testuser";
    client.insecure_server = insecure;

    client.connect()
}
